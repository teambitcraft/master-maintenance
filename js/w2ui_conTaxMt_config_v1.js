var conTaxMt_grid_config = {
    name: 'conTaxMtgmain',
    show: {
        toolbar: true,
        footer: true,
        lineNumbers: true
    },
    columns: [
        {field: 'recid', caption: 'recid', size: '60px', sortable: true, resizable: true,hidden: true},
        {field: 'id', caption: 'ID', size: '150px', sortable: true, resizable: true, hidden: true},
        {field: 'histNo', caption: '履歴No', size: '30%', sortable: true, resizable: true},
        {field: 'startDate', caption: '適用開始年月日', size: '70%', sortable: true, resizable: true, render: 'date'},
        {field: 'percent', caption: '消費税率', size: '120px', sortable: true, resizable: true, render: 'percent'}
    ],
    /*************************************************************************
     ************************* toolbar *************************************
     *************************************************************************/
    toolbar: {
        items: [
            {id: 'add', type: 'button', caption: 'データ行追加', icon: 'w2ui-icon-plus'},
            {id: 'remove', type: 'button', caption: 'データ行削除', icon: 'w2ui-icon-cross'}
        ],
        onClick: function (event) {
            /*************************************************************************
            ************************* grid add event *************************************
            *************************************************************************/
            if (event.target == 'add') {
                var max_recid = 0;
                for (var num in w2ui.conTaxMtgmain.records) {
                    //console.log('num: ',num);
                    //console.log('content: ', w2ui.conTaxMtgmain.records[num]);
                    //console.log('recid:', w2ui.conTaxMtgmain.records[num].recid);
                    var recid_num = w2ui.conTaxMtgmain.records[num].recid;
                    if(max_recid < recid_num) {
                        max_recid = recid_num;
                    }
                }
                //console.log('max', max_recid);

                w2ui.conTaxMtgmain.add({recid: Number(max_recid) + 1});
                var json_data = w2ui.conTaxMtgmain.get(Number(max_recid)+1);
                console.log(json_data);
                $.ajax({
                    url: "/conTaxMt_addButton",
                    type: "POST",
                    async:false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(json_data),
                    success: function(data) {
                        //console.log('data: ', data);
                        console.log(data);
                        console.log(json_data);
                        json_data.id = data;
                        console.log(json_data);
                        // ページをリロードする感じ
                        w2ui.conTaxMtgmain.refresh();
                        w2ui.conTaxMtgmain.selectNone();
                        //for (var i in data) {
                        //    console.log(i + ' : ' +  data[i]);
                        //}
                    }
                });
            }

            /*************************************************************************
            ************************* grid remove event *****************************
            *************************************************************************/
            // removeボタンがおされたとき
            if (event.target == 'remove') {
                // selectには、選択されているgridのrecidが配列として、格納される。
                var select = w2ui.conTaxMtgmain.getSelection();
                console.log('sel.len: ', select.length);
                // 選択されていなかったら、selectの長さは0
                if(select.length == 1) {
                    var res = confirm('消去しますか？');
                    // OKボタンが押されたら
                    if(res == true) {
                        var sel = w2ui.conTaxMtgmain.getSelection();
                        console.log('sel', sel);
                        console.log('sel[0]: ', sel[0]);
                        var json_data = w2ui.conTaxMtgmain.get(sel[0]);
                        console.log('json_data: ', json_data);
                        var id = json_data.id;
                        console.log('send_id: ', id);
                        w2ui.conTaxMtgmain.remove(sel[0]);
                        $.ajax({
                            url: "/conTaxMt_removeButton",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify(id),
                            success: function (data) {
                                //console.log('data: ', data);
                                console.log('deleted_data: ', data);
                                //for (var i in data) {
                                //    console.log(i + ' : ' +  data[i]);
                                //}
                            }
                        });
                        // 最後にフォームの内容をクリア
                        w2ui.conTaxMtfmain.clear();
                    }
                }
            }
        }
    },
    //records: [],
    /*************************************************************************
     ************************* grid click event *************************************
     *************************************************************************/
    onClick: function(event) {
        var grid = this;
        var form = w2ui.conTaxMtfmain;
        //console.log(event);
        event.onComplete = function() {
            var sel = grid.getSelection();
            //console.log(sel);
            if (sel.length == 1) {
                form.recid = sel[0];
                //alert('recid:' + form.recid);
                //console.log(form.recid);
                form.record = $.extend(true, {}, grid.get(sel[0]));
                form.refresh();
            }
            else {
                form.clear();
            }
        };
    }
};
var conTaxMt_form_config = {
    name: 'conTaxMtfmain',
    fields: [
        {name: 'recid', type: 'text', html: { caption: 'recid', attr: 'style="width:130px" readonly'}},
        {name: 'id', type: 'text', html: { caption: 'gaeid', attr: 'style="width:130px" readonly'}},
        {name: 'histNo', type: 'text', html: { caption: '履歴No', attr: 'style="width:130px"'}},
        {name: 'startDate', type: 'date', html: { caption: '適用開始年月日', attr: 'style="width:130px"'}},
        {name: 'percent', type: 'text', html: { caption: '消費税率', attr: 'style="width:130px"'}}
    ],
    actions: {
        /*************************************************************************
         ************************* form reset *************************************
         *************************************************************************/
        Reset: function () {
            this.clear();
        },
        /*************************************************************************
         ************************* form save *************************************
         *************************************************************************/
        Save: function () {
            var errors = this.validate();
            if (errors.length > 0) return;

            if (this.recid != 0) {
                //w2ui.conTaxMtgmain.add({recid: w2ui.conTaxMtgmain.records.length+1});
                //console.log('recid: ' + this.recid + '\nrecord: ' + this.record);
                w2ui.conTaxMtgmain.set(this.recid, this.record);
                w2ui.conTaxMtgmain.selectNone();
                //console.log('saved target: ', this.record);
                //for (var i in this.record) {
                //    console.log('content: ',this.record[i]);
                //}

                var json_data = this.record;
                console.log('json_data: ', json_data);
                $.ajax({
                    url: "/conTaxMt_saveButton",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(json_data),
                    success: function (data) {
                        //console.log('data: ', data);
                        console.log('this data is saved: ', data);
                        //for (var i in data) {
                        //    console.log(i + ' : ' +  data[i]);
                        //}
                    }
                });
                this.clear();
            } else {
                alert('行を選択してください。');
            }
        }
    }
};


// config["grid"] = grid_config;
// config["form"] = form_config;
// grid_config.toolbar.onClick = grid_toolbar_click;
// config["grid"] = grid_config;
// config["form"] = form_config;