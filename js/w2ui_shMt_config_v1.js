var shMt_grid_config = {
    name: 'shMtgmain',
    show: {
        toolbar: true,
        footer: true,
        lineNumbers: true
    },
    columns: [
        {field: 'recid', caption: 'recid', size: '60px', sortable: true, resizable: true, hidden: true},
        {field: 'id', caption: 'ID', size: '150px', sortable: true, resizable: true, hidden: true},
        //{field: 'id', caption: 'ID', size: '150px', sortable: true, resizable: true, hidden:true},
        //{field: 'shKubunCode', caption: '商品区分コード', size: '30%', sortable: true, resizable: true},
        {field: 'shCode', caption: '商品コード', size: '20%', sortable: true, resizable: true},
        {field: 'shName', caption: '商品名', size: '50%', sortable: true, resizable: true},
        {field: 'makerName', caption: 'メーカー名', size: '30%', sortable: true, resizable: true}
    ],
    /*************************************************************************
     ************************* toolbar *************************************
     *************************************************************************/
    toolbar: {
        items: [
            {id: 'add', type: 'button', caption: 'データ行追加', icon: 'w2ui-icon-plus'},
            {id: 'remove', type: 'button', caption: 'データ行削除', icon: 'w2ui-icon-cross'}
        ],
        onClick: function (event) {
            /*************************************************************************
            ************************* grid add event *************************************
            *************************************************************************/
            if (event.target == 'add') {
                var max_recid = 0;
                for (var num in w2ui.shMtgmain.records) {
                    //console.log('num: ',num);
                    //console.log('content: ', w2ui.shMtgmain.records[num]);
                    //console.log('recid:', w2ui.shMtgmain.records[num].recid);
                    var recid_num = w2ui.shMtgmain.records[num].recid;
                    if(max_recid < recid_num) {
                        max_recid = recid_num;
                    }
                }
                //console.log('max', max_recid);
        
                w2ui.shMtgmain.add({recid: Number(max_recid) + 1});
                var json_data = w2ui.shMtgmain.get(Number(max_recid)+1);
                console.log(json_data);
                $.ajax({
                    url: "/shMt_addButton",
                    type: "POST",
                    async:false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(json_data),
                    success: function(data) {
                        //console.log('data: ', data);
                        console.log(data);
                        console.log(json_data);
                        json_data.id = data;
                        console.log(json_data);
                        // ページをリロードする感じ
                        w2ui.shMtgmain.refresh();
                        w2ui.shMtgmain.selectNone();
                        //for (var i in data) {
                        //    console.log(i + ' : ' +  data[i]);
                        //}
                    }
                });
            }
        
            /*************************************************************************
            ************************* grid remove event *****************************
            *************************************************************************/
            // removeボタンがおされたとき
            if (event.target == 'remove') {
                // selectには、選択されているgridのrecidが配列として、格納される。
                var select = w2ui.shMtgmain.getSelection();
                console.log('sel.len: ', select.length);
                // 選択されていなかったら、selectの長さは0
                if(select.length == 1) {
                    var res = confirm('消去しますか？');
                    // OKボタンが押されたら
                    if(res == true) {
                        var sel = w2ui.shMtgmain.getSelection();
                        console.log('sel', sel);
                        console.log('sel[0]: ', sel[0]);
                        var json_data = w2ui.shMtgmain.get(sel[0]);
                        console.log('json_data: ', json_data);
                        var id = json_data.id;
                        console.log('send_id: ', id);
                        w2ui.shMtgmain.remove(sel[0]);
                        $.ajax({
                            url: "/shMt_removeButton",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify(id),
                            success: function (data) {
                                //console.log('data: ', data);
                                console.log('deleted_data: ', data);
                                //for (var i in data) {
                                //    console.log(i + ' : ' +  data[i]);
                                //}
                            }
                        });
                        // 最後にフォームの内容をクリア
                        w2ui.shMtfmain.clear();
                    }
                }
            }
        }
    },
    //records: [],
    /*************************************************************************
     ************************* grid click event *************************************
     *************************************************************************/
    onClick: function(event) {
        var grid = this;
        var form = w2ui.shMtfmain;
        //console.log(event);
        event.onComplete = function() {
            var sel = grid.getSelection();
            //console.log(sel);
            if (sel.length == 1) {
                form.recid = sel[0];
                //alert('recid:' + form.recid);
                //console.log(form.recid);
                form.record = $.extend(true, {}, grid.get(sel[0]));
                form.refresh();
            }
            else {
                form.clear();
            }
        };
    }
};
var shMt_form_config = {
    name: 'shMtfmain',
    fields: [
        {name: 'recid', type: 'text', html: { caption: 'recid', attr: 'style="width:130px" readonly'}},
        {name: 'id', type: 'text', html: { caption: 'gaeid', attr: 'style="width:130px" readonly'}},
        {name: 'shKubunCode', type: 'list', required: true, html: { caption: '商品区分コード', attr: 'style="width: 200px"'}},
        {name: 'shCode', type: 'text', required: true, html: { caption: '商品コード', attr: 'style="width: 200px"'}},
        {name: 'sharyoNo', type: 'text', html: { caption: '車両番号', attr: 'style="width: 200px"'}},
        {name: 'shName', type: 'text', html: { caption: '商品名', attr: 'style="width: 200px"'}},
        {name: 'unit', type: 'list', required: true, html: { caption: '単位', attr: 'style="width: 100px"'},},
        {name: 'makerName', type: 'text', html: { caption: 'メーカー名', attr: 'style="width: 200px"'}},
        {name: 'kataShiki', type: 'text', html: { caption: '形式', attr: 'style="width: 200px"'}},
        {name: 'seizoNo', type: 'text', html: { caption: '製造番号', attr: 'style="width: 200px"'}},
        {name: 'shisanNo', type: 'text', html: { caption: '資産番号', attr: 'style="width: 200px"'}},
        {name: 'ryakuName_1', type: 'text', html: { caption: '略称-1', attr: 'style="width: 200px"'}},
        {name: 'ryakuName_2', type: 'text', html: { caption: '略称-2', attr: 'style="width: 200px"'}},
        {name: 'inputKubun', type: 'list', required: true, html: { caption: '入力区分', attr: 'style="width: 200px"'}, options: { items: ['数量✕金額', '金額直接入力'] }},
        {name: 'memo', type: 'textarea', html: { caption: 'メモ', attr: 'style="width: 200px; height=100px"'}},
        {name: 'dayStdPrice', type: 'text', html: { caption: '昼間・標準単価', attr: 'style="width: 200px"'}},
        {name: 'dayTanka_1', type: 'text', html: { caption: '昼間・単価-1', attr: 'style="width: 200px"'}},
        {name: 'dayTanka_2', type: 'text', html: { caption: '昼間・単価-2', attr: 'style="width: 200px"'}},
        {name: 'dayTanka_3', type: 'text', html: { caption: '昼間・単価-3', attr: 'style="width: 200px"'}},
        {name: 'dayTanka_4', type: 'text', html: { caption: '昼間・単価-4', attr: 'style="width: 200px"'}},
        {name: 'dayTanka_5', type: 'text', html: { caption: '昼間・単価-5', attr: 'style="width: 200px"'}},
        {name: 'eveStdPrice', type: 'text', html: { caption: '夜間・標準単価', attr: 'style="width: 200px"'}},
        {name: 'eveTanka_1', type: 'text', html: { caption: '夜間・単価-1', attr: 'style="width: 200px"'}},
        {name: 'eveTanka_2', type: 'text', html: { caption: '夜間・単価-2', attr: 'style="width: 200px"'}},
        {name: 'eveTanka_3', type: 'text', html: { caption: '夜間・単価-3', attr: 'style="width: 200px"'}},
        {name: 'eveTanka_4', type: 'text', html: { caption: '夜間・単価-4', attr: 'style="width: 200px"'}},
        {name: 'eveTanka_5', type: 'text', html: { caption: '夜間・単価-5', attr: 'style="width: 200px"'}}
    ],
    actions: {
        /*************************************************************************
         ************************* form reset *************************************
         *************************************************************************/
        Reset: function () {
            this.clear()
        },
        /*************************************************************************
         ************************* form save *************************************
         *************************************************************************/
        Save: function () {
            var errors = this.validate();
            if (errors.length > 0) return;

            if (this.recid != 0) {
                //w2ui.shMtgmain.add({recid: w2ui.shMtgmain.records.length+1});
                //console.log('recid: ' + this.recid + '\nrecord: ' + this.record);
                w2ui.shMtgmain.set(this.recid, this.record);
                w2ui.shMtgmain.selectNone();
                //console.log('saved target: ', this.record);
                //for (var i in this.record) {
                //    console.log('content: ',this.record[i]);
                //}

                var json_data = this.record;
                console.log('json_data: ', json_data);
                $.ajax({
                    url: "/shMt_saveButton",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(json_data),
                    success: function (data) {
                        //console.log('data: ', data);
                        console.log('this data is saved: ', data);
                        //for (var i in data) {
                        //    console.log(i + ' : ' +  data[i]);
                        //}
                    }
                });
                this.clear();
            } else {
                alert('行を選択してください。');
            }
        }
    }
};


// config["grid"] = grid_config;
// config["form"] = form_config;
// grid_config.toolbar.onClick = grid_toolbar_click;
// config["grid"] = grid_config;
// config["form"] = form_config;