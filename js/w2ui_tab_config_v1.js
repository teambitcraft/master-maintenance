w2utils.locale('../json/ja-jp.json');
var config = {
	layout: {
        name: 'lmain',
        panels: [
            { type: 'left', content: 'left', size: '200px', resizable: true },
            { type: 'main', content: 'main', size: '100%', overflow:'hidden', resizable: true },		
            { type: 'right', content: 'right', size: '400px', overflow:'hidden', resizable: true }
        ]
    },
	sidebar: {
		name: 'sidebar',
		nodes: [ 
			{ id: 'masterMt', text: 'マスタメンテナンス', group: true, expanded: true, nodes: [
				{ id: 'tkMt', text: '得意先', img: 'icon-folder', selected: true },
				{ id: 'tantoMt', text: '担当', img: 'icon-folder' },
				{ id: 'shMt', text: '商品', img: 'icon-folder' },
				{ id: 'shKubunMt', text: '商品区分', img: 'icon-folder' },
				{ id: 'unitMt', text: '単位', img: 'icon-folder' },
				{ id: 'operMt', text: '作業者', img: 'icon-folder' },
				{ id: 'conTaxMt', text: '消費税', img: 'icon-folder' },
				{ id: 'tantoMt6', text: 'カレンダ', img: 'icon-page' },
				{ id: 'sysEnvMt', text: 'システム環境', img: 'icon-page' }
			]}
		],
		onClick: function (event) {
			switch (event.target) {
				case 'tkMt':
					w2ui.lmain.content('main', w2ui.tkMtgmain);
					w2ui.lmain.content('right', w2ui.tkMtfmain);
					break;
				case 'tantoMt':
					w2ui.lmain.content('main', w2ui.tantoMtgmain);
					w2ui.lmain.content('right', w2ui.tantoMtfmain);
					break;
                case 'shMt':
                    w2ui.lmain.content('main', w2ui.shMtgmain);
                    w2ui.lmain.content('right', w2ui.shMtfmain);
                    break;
                case 'shKubunMt':
					w2ui.lmain.content('main', w2ui.shKubunMtgmain);
					w2ui.lmain.content('right', w2ui.shKubunMtfmain);
                    break;
                case 'unitMt':
                    w2ui.lmain.content('main', w2ui.unitMtgmain);
                    w2ui.lmain.content('right', w2ui.unitMtfmain);
                    break;
                case 'operMt':
                    w2ui.lmain.content('main', w2ui.operMtgmain);
                    w2ui.lmain.content('right', w2ui.operMtfmain);
                    break;
                case 'conTaxMt':
                    w2ui.lmain.content('main', w2ui.conTaxMtgmain);
                    w2ui.lmain.content('right', w2ui.conTaxMtfmain);
                    break;
                case 'sysEnvMt':
                    w2ui.lmain.content('main', w2ui.sysEnvMtgmain);
                    w2ui.lmain.content('right', w2ui.sysEnvMtfmain);
                    break;
			}
		}
	}
};

