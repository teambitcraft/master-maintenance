var config = {
    layout: {
        name: 'lmain',
        panels: [
            { type: 'main', content: 'main', size: '50%', resizable: true },
            { type: 'left', content: 'left', size: '50%', resizable: true }
        ]
    },
    grid: {
        name: 'tkMttkMtgmain',
        columns: [
            {field: 'code', caption: 'First Name', size: '30%'},
            {field: 'company', caption: 'Last Name', size: '30%'},
            {field: 'adress', caption: 'Email', size: '40%'},
            {field: 'money', caption: 'Start Date', size: '120px'}
        ],
        records:[],
        onClick: function(event) {
            var grid = this;
            var form = w2ui.tkMtfmain;
            //console.log(event);
            event.onComplete = function() {
                var sel = grid.getSelection();
                if (sel.length == 1) {
                    form.recid = sel[0];
                    console.log('form.recid:', sel[0]);
                    form.record = $.extend(true, {}, grid.get(sel[0]));
                    form.refresh();
                }
                else {
                    form.clear();
                }
            };
        }
    },
    form: {
        header: 'Edit Record',
        name: 'tkMtfmain',
        fields: [
            { name: 'recid', type: 'text', html: { caption: 'ID', attr: 'size="10" readonly' } },
            { name: 'code', type: 'text', required: true, html: { caption: 'First Name', attr: 'size="40" maxlength="40"' } },
            { name: 'company', type: 'text', required: true, html: { caption: 'Last Name', attr: 'size="40" maxlength="40"' } },
            { name: 'adress', type: 'email', html: { caption: 'Email', attr: 'size="30"' } },
            { name: 'money', type: 'date', html: { caption: 'Date', attr: 'size="10"' } }
        ],
        actions: {
            Reset: function() {
        //        this.clear();
            },
            Save: function() {
                //var errors = this.validate();
                //if (errors.length > 0) {
                //    return;
                //}
                //if (this.recid == 0) {
                //    w2ui.tkMttkMtgmain.add($.extend(true, { recid: w2ui.tkMttkMtgmain.records.length + 1 }, this.record));
                //    w2ui.tkMttkMtgmain.selectNone();
                //    this.clear();
                //}
                //else {
                //    w2ui.tkMttkMtgmain.set(this.recid, this.record);
                //    w2ui.tkMttkMtgmain.selectNone();
                //    this.clear();
                //}
            }
        }
    }
};
$(function() {
    $("#main").w2layout(config.layout);
    $().w2grid(config.grid);
    $().w2form(config.form);
    w2ui.tkMttkMtgmain.load('../json/records.json');
    w2ui.lmain.content('left', w2ui.tkMttkMtgmain);
    w2ui.lmain.content('main', w2ui.tkMtfmain);
});
