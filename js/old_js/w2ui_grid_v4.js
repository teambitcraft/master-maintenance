/**
 *
 * Created by k_nakamura on 11/15/15.
 */
var config = {
    layout: {
        name: 'lmain',
        panels: [
            { type: 'main', content: 'main', size: '40%', resizable: true },
            { type: 'left', content: 'left', size: '60%', resizable: true }
        ]
    },
    grid: {
        name: 'tkMtgmain',
        show: {
            toolbar: true,
            footer: true
        },
        columns: [
            {field: 'recid', caption: 'ID', size: '10%', sortable: true, resizable: true},
            {field: 'company', caption: '得意先名', size: '40%', sortable: true, resizable: true},
            {field: 'adress', caption: '得意先住所', size: '40%', sortable: true, resizable: true},
            {field: 'money', caption: '請求金額', size: '120px', sortable: true, resizable: true, render: 'money'}
        ],
        toolbar: {
            items: [
                {id: 'add', type: 'button', caption: 'Add Record', icon: 'w2ui-icon-plus'},
                {id: 'remove', type: 'button', caption: 'Remove Record', icon: 'w2ui-icon-cross'}
            ],
            onClick: function (event) {
                if (event.target == 'add') {
                    var max_recid = 0;
                    for (var num in w2ui.tkMtgmain.records) {
                        //console.log('num: ',num);
                        //console.log('content: ', w2ui.tkMtgmain.records[num]);
                        //console.log('recid:', w2ui.tkMtgmain.records[num].recid);
                        recid_num = w2ui.tkMtgmain.records[num].recid;
                        if(max_recid < recid_num) {
                            max_recid = recid_num;
                        }
                    }
                    //console.log('max', max_recid);
                    w2ui.tkMtgmain.add({recid: Number(max_recid) + 1});


                    var json_data = w2ui.tkMtgmain.get(Number(max_recid)+1);
                    console.log(json_data);
                    $.ajax({
                        url: "/addButton",
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(json_data),
                        success: function(data) {
                            //console.log('data: ', data);
                            console.log('data.recid: ', data.recid);
                            //for (var i in data) {
                            //    console.log(i + ' : ' +  data[i]);
                            //}
                        }
                    });
                }
                if (event.target == 'remove') {
                    var sel = w2ui.tkMtgmain.getSelection();
                    //console.log(sel[0]);
                    var json_data = w2ui.tkMtgmain.get(sel[0]);
                    w2ui.tkMtgmain.remove(sel[0]);
                    $.ajax({
                        url: "/removeButton",
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(json_data),
                        success: function(data) {
                            //console.log('data: ', data);
                            console.log('data.recid: ', data.recid);
                            //for (var i in data) {
                            //    console.log(i + ' : ' +  data[i]);
                            //}
                        }
                    });
                }
            }
        },
        records: [],
        onClick: function(event) {
            var grid = this;
            var form = w2ui.tkMtfmain;
            //console.log(event);
            event.onComplete = function() {
                var sel = grid.getSelection();
                //console.log(sel);
                if (sel.length == 1) {
                    form.recid = sel[0];
                    //alert('recid:' + form.recid);
                    //console.log(form.recid);
                    form.record = $.extend(true, {}, grid.get(sel[0]));
                    form.refresh();
                }
                else {
                    form.clear();
                }
            };
        }
    },
    form: {
        name: 'tkMtfmain',
        fields: [
            {name: 'id', type: 'hidden', value: "{{data.key.id}}"},
            {name: 'recid', type: 'text'},
            {name: 'company', type: 'text', required: true},
            {name: 'adress', type: 'text'},
            {name: 'money', type: 'text'}
        ],
        actions: {
            reset: function () {
                this.clear();
            },
            save: function () {
                var errors = this.validate();
                if (errors.length > 0) return;

                if (this.recid != 0) {
                    //w2ui.tkMtgmain.add({recid: w2ui.tkMtgmain.records.length+1});
                    //console.log('recid: ' + this.recid + '\nrecord: ' + this.record);
                    w2ui.tkMtgmain.set(this.recid, this.record);
                    w2ui.tkMtgmain.selectNone();
                }
                //console.log('saved target: ', this.record);
                //for (var i in this.record) {
                //    console.log('content: ',this.record[i]);
                //}

                var json_data = this.record;
                console.log('json_data: ', json_data);
                for (var i in json_data) {
                    //console.log(i + ': ' + json_data[i]);
                }
                //json_data = {recid:'sample', data:'sample_data'};
                //$.ajax({
                //    url: "/savebutton",
                //    type: "POST",
                //    dataType: "json",
                //    contentType: "application/json; charset=utf-8",
                //    data: JSON.stringify(json_data),
                //    success: function(data) {
                //        //console.log('data: ', data);
                //        console.log('data.money: ', data.money);
                //        //for (var i in data) {
                //        //    console.log(i + ' : ' +  data[i]);
                //        //}
                //    }
                //});
                this.clear();
            }
        }
    }
};
$(function() {
    $(window).load(function(){
       $.ajax({
            url: "/",
            type: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json_data),
            success: function(data) {
                w2ui.tkMtgmain.records = data;
            }
        });
    });
    $("#main").w2layout(config.layout);
    $().w2grid(config.grid);
    // read json file from local folder
    //w2ui.tkMtgmain.load('../json/records.json');

    //var $form = $('#myForm').w2form(config.form);
    //$().w2form(config.form);
    w2ui.lmain.content('left', w2ui.tkMtgmain);
    w2ui.lmain.content('main', $('#myForm').w2form(config.form));
});

