/**
 * Created by k_nakamura on 11/14/15.
 */
var config = {
    layout: {
        name: 'lmain',
        panels: [
            { type: 'main', content: 'main', size: '40%', resizable: true },
            { type: 'left', content: 'left', size: '60%', resizable: true }
        ]
    },
    grid: {
        name: 'tkMtgmain',
        columns: [
            {field: 'code', caption: '得意先コード', size: '30%'},
            {field: 'company', caption: '得意先名', size: '30%'},
            {field: 'adress', caption: '住所', size: '40%'},
            {field: 'money', caption: '請求金額', size: '120px'}
        ],
        records:[],
        onClick: function(event) {
            var grid = this;
            var form = w2ui.tkMtfmain;
            console.log(event);
            event.onComplete = function() {
                var sel = grid.getSelection();
                console.log(sel[0]);
                if (sel.length == 1) {
                    form.recid = sel[0];
                    //alert('recid:' + form.recid);
                    form.record = $.extend(true, {}, grid.get(sel[0]));
                    form.refresh();
                }
                else {
                    form.clear();
                }
            };
        }
    },
    form: {
        name: 'tkMtfmain',
        fields: [
            {name: 'recid', type: 'text'},
            {name: 'code', type: 'text', required: true},
            {name: 'company', type: 'text', required: true},
            {name: 'adress', type: 'text'},
            {name: 'money', type: 'text'}
        ],
        actions: {
            reset: function () {
                this.clear();
            },
            save: function () {
                var errors = this.validate();
                if (errors.length > 0) return;
                if (this.recid == 0) {
                    //alert(w2ui.tkMtgmain.records.length + 1);
                    w2ui.tkMtgmain.add($.extend(true, { recid: w2ui.tkMtgmain.records.length+1 }, this.record));
                    w2ui.tkMtgmain.selectNone();
                    this.clear();
                } else {
                    w2ui.tkMtgmain.set(this.recid, this.record);
                    w2ui.tkMtgmain.selectNone();
                    this.clear();
                }
            }
        }
    }
};
$(function() {
    $("#main").w2layout(config.layout);
    $().w2grid(config.grid);
    w2ui.tkMtgmain.load('../json/records.json');
    //var $form = $('#myForm').w2form(config.form);
    //$().w2form(config.form);
    w2ui.lmain.content('left', w2ui.tkMtgmain);
    w2ui.lmain.content('main', $('#myForm').w2form(config.form));
});
