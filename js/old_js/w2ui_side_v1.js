var config_side = {};
$.getScript("./w2ui_side_config_v1.js", function(){
    config_side = config;
});
$(function () {
    $('#main').w2layout(config_side.layout);
    w2ui.layout.content('left', $().w2sidebar(config_side.sidebar));
    w2ui.layout.content('main', $().w2grid(config_side.grid1));
    // in memory initialization
    $().w2grid(config_side.grid2);
});
