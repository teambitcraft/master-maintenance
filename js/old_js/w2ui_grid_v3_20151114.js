/**
 * Created by k_nakamura on 11/15/15.
 */
/**
 * Created by k_nakamura on 11/14/15.
 */
var config = {
    layout: {
        name: 'lmain',
        panels: [
            { type: 'main', content: 'main', size: '40%', resizable: true },
            { type: 'left', content: 'left', size: '60%', resizable: true }
        ]
    },
    grid: {
        name: 'tkMtgmain',
        show: {
            toolbar: true,
            footer: true
        },
        columns: [
            {field: 'recid', caption: 'ID', size: '10%', sortable: true, resizable: true},
            {field: 'code', caption: 'code', size: '10%', sortable: true, resizable: true},
            {field: 'company', caption: '得意先名', size: '40%', sortable: true, resizable: true},
            {field: 'adress', caption: '得意先住所', size: '40%', sortable: true, resizable: true},
            {field: 'money', caption: '請求金額', size: '120px', sortable: true, resizable: true, render: 'money'}
        ],
        toolbar: {
            items: [
                {id: 'add', type: 'button', caption: 'Add Record', icon: 'w2ui-icon-plus'},
                {id: 'remove', type: 'button', caption: 'Remove Record', icon: 'w2ui-icon-cross'}
            ],
            onClick: function (event) {
                if (event.target == 'add') {
                    w2ui.tkMtgmain.add({recid: w2ui.tkMtgmain.records.length + 1});
                }
                if (event.target == 'remove') {
                    var sel = w2ui.tkMtgmain.getSelection();
                    console.log(sel[0]);
                    w2ui.tkMtgmain.remove(sel[0]);
                }
            }
        },
        records:[],
        onClick: function(event) {
            var grid = this;
            var form = w2ui.tkMtfmain;
            console.log(event);
            event.onComplete = function() {
                var sel = grid.getSelection();
                console.log(sel[0]);
                if (sel.length == 1) {
                    form.recid = sel[0];
                    //alert('recid:' + form.recid);
                    form.record = $.extend(true, {}, grid.get(sel[0]));
                    form.refresh();
                }
                else {
                    form.clear();
                }
            };
        }
    },
    form: {
        name: 'tkMtfmain',
        fields: [
            {name: 'recid', type: 'text'},
            {name: 'code', type: 'text', required: true},
            {name: 'company', type: 'text', required: true},
            {name: 'adress', type: 'text'},
            {name: 'money', type: 'text'}
        ],
        actions: {
            reset: function () {
                this.clear();
            },
            save: function () {
                var errors = this.validate();
                if (errors.length > 0) return;
                if (this.recid != 0) {
                    //w2ui.tkMtgmain.add({recid: w2ui.tkMtgmain.records.length+1});
                    w2ui.tkMtgmain.set(this.recid, this.record);
                    w2ui.tkMtgmain.selectNone();
                    this.clear();
                }
            }
        }
    }
};
$(function() {
    $("#main").w2layout(config.layout);
    $().w2grid(config.grid);
    w2ui.tkMtgmain.load('../json/records.json');
    //var $form = $('#myForm').w2form(config.form);
    //$().w2form(config.form);
    w2ui.lmain.content('left', w2ui.tkMtgmain);
    w2ui.lmain.content('main', $('#myForm').w2form(config.form));
});
