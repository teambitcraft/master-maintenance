var config = {
    layout: {
        name: 'layout',
        padding: 0,
        panels: [
            { type: 'left', size: 200, resizable: true, minSize: 120 },
            { type: 'main', minSize: 550, overflow: 'hidden' }
        ]
    },
    sidebar: {
        name: 'sidebar',
        nodes: [
            { id: 'general', text: 'General', group: true, expanded: true, nodes: [
                { id: 'grid1', text: 'Grid 1', img: 'icon-page', selected: true },
                { id: 'grid2', text: 'Grid 2', img: 'icon-page' },
                { id: 'html', text: 'Some HTML', img: 'icon-page' }
            ]}
        ],
        onClick: function (event) {
            switch (event.target) {
                case 'grid1':
                    w2ui.layout.content('main', w2ui.grid1);
                    break;
                case 'grid2':
                    w2ui.layout.content('main', w2ui.grid2);
                    break;
                case 'html':
                    w2ui.layout.content('main', '<div style="padding: 10px">Some HTML</div>');
                    $(w2ui.layout.el('main'))
                        .removeClass('w2ui-grid')
                        .css({
                            'border-left': '1px solid silver'
                        });
                    break;
            }
        }
    },
    grid1: {
        name: 'grid1',
        show: {
            toolbar: true,
            footer: true
        },
        columns: [
            {field: 'recid', caption: 'recid', size: '10%', sortable: true, resizable: true},
            {field: 'id', caption: 'ID', size: '150px', sortable: true, resizable: true},
            {field: 'company', caption: '得意先名', size: '40%', sortable: true, resizable: true},
            {field: 'address', caption: '得意先住所', size: '50%', sortable: true, resizable: true},
            {field: 'money', caption: '請求金額', size: '120px', sortable: true, resizable: true, render: 'money'}
        ],
        /*************************************************************************
         ************************* toolbar *************************************
         *************************************************************************/
        toolbar: {
            items: [
                {id: 'add', type: 'button', caption: 'Add Record', icon: 'w2ui-icon-plus'},
                {id: 'remove', type: 'button', caption: 'Remove Record', icon: 'w2ui-icon-cross'}
            ],

            onClick: function (event) {
                /*************************************************************************
                 ************************* grid add event *************************************
                 *************************************************************************/
                if (event.target == 'add') {
                    var max_recid = 0;
                    for (var num in w2ui.tkMtgmain.records) {
                        //console.log('num: ',num);
                        //console.log('content: ', w2ui.tkMtgmain.records[num]);
                        //console.log('recid:', w2ui.tkMtgmain.records[num].recid);
                        var recid_num = w2ui.tkMtgmain.records[num].recid;
                        if(max_recid < recid_num) {
                            max_recid = recid_num;
                        }
                    }
                    //console.log('max', max_recid);

                    w2ui.tkMtgmain.add({recid: Number(max_recid) + 1});
                    var json_data = w2ui.tkMtgmain.get(Number(max_recid)+1);
                    console.log(json_data);
                    $.ajax({
                        url: "/addButton",
                        type: "POST",
                        async:false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(json_data),
                        success: function(data) {
                            //console.log('data: ', data);
                            console.log(data);
                            console.log(json_data);
                            json_data.id = data;
                            console.log(json_data);
                            // ページをリロードする感じ
                            w2ui.tkMtgmain.refresh();
                            w2ui.tkMtgmain.selectNone();
                            //for (var i in data) {
                            //    console.log(i + ' : ' +  data[i]);
                            //}
                        }
                    });
                }

                /*************************************************************************
                 ************************* grid remove event *****************************
                 *************************************************************************/
                // removeボタンがおされたとき
                if (event.target == 'remove') {
                    // selectには、選択されているgridのrecidが配列として、格納される。
                    var select = w2ui.tkMtgmain.getSelection();
                    console.log('sel.len: ', select.length);
                    // 選択されていなかったら、selectの長さは0
                    if(select.length == 1) {
                        var res = confirm('消去しますか？');
                        // OKボタンが押されたら
                        if(res == true) {
                            var sel = w2ui.tkMtgmain.getSelection();
                            console.log('sel', sel);
                            console.log('sel[0]: ', sel[0]);
                            var json_data = w2ui.tkMtgmain.get(sel[0]);
                            console.log('json_data: ', json_data);
                            var id = json_data.id;
                            console.log('send_id: ', id);
                            w2ui.tkMtgmain.remove(sel[0]);
                            $.ajax({
                                url: "/removeButton",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                data: JSON.stringify(id),
                                success: function (data) {
                                    //console.log('data: ', data);
                                    console.log('deleted_data: ', data);
                                    //for (var i in data) {
                                    //    console.log(i + ' : ' +  data[i]);
                                    //}
                                }
                            });
                            // 最後にフォームの内容をクリア
                            w2ui.tkMtfmain.clear();
                        }
                    }
                }
            }
        },
        records: [],
        /*************************************************************************
         ************************* grid click event *************************************
         *************************************************************************/
        onClick: function(event) {
            var grid = this;
            var form = w2ui.tkMtfmain;
            //console.log(event);
            event.onComplete = function() {
                var sel = grid.getSelection();
                //console.log(sel);
                if (sel.length == 1) {
                    form.recid = sel[0];
                    //alert('recid:' + form.recid);
                    //console.log(form.recid);
                    form.record = $.extend(true, {}, grid.get(sel[0]));
                    form.refresh();
                }
                else {
                    form.clear();
                }
            };
        }
    },
    grid2: {
        name: 'grid2',
        show: {
            toolbar: true,
            footer: true
        },
        columns: [
            {field: 'recid', caption: 'recid', size: '10%', sortable: true, resizable: true},
            {field: 'id', caption: 'ID', size: '150px', sortable: true, resizable: true},
            {field: 'company', caption: '得意先名', size: '40%', sortable: true, resizable: true},
            {field: 'address', caption: '得意先住所', size: '50%', sortable: true, resizable: true},
            {field: 'money', caption: '請求金額', size: '120px', sortable: true, resizable: true, render: 'money'}
        ],
        /*************************************************************************
         ************************* toolbar *************************************
         *************************************************************************/
        toolbar: {
            items: [
                {id: 'add', type: 'button', caption: 'Add Record', icon: 'w2ui-icon-plus'},
                {id: 'remove', type: 'button', caption: 'Remove Record', icon: 'w2ui-icon-cross'}
            ],

            onClick: function (event) {
                /*************************************************************************
                 ************************* grid add event *************************************
                 *************************************************************************/
                if (event.target == 'add') {
                    var max_recid = 0;
                    for (var num in w2ui.tkMtgmain.records) {
                        //console.log('num: ',num);
                        //console.log('content: ', w2ui.tkMtgmain.records[num]);
                        //console.log('recid:', w2ui.tkMtgmain.records[num].recid);
                        var recid_num = w2ui.tkMtgmain.records[num].recid;
                        if(max_recid < recid_num) {
                            max_recid = recid_num;
                        }
                    }
                    //console.log('max', max_recid);

                    w2ui.tkMtgmain.add({recid: Number(max_recid) + 1});
                    var json_data = w2ui.tkMtgmain.get(Number(max_recid)+1);
                    console.log(json_data);
                    $.ajax({
                        url: "/addButton",
                        type: "POST",
                        async:false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(json_data),
                        success: function(data) {
                            //console.log('data: ', data);
                            console.log(data);
                            console.log(json_data);
                            json_data.id = data;
                            console.log(json_data);
                            // ページをリロードする感じ
                            w2ui.tkMtgmain.refresh();
                            w2ui.tkMtgmain.selectNone();
                            //for (var i in data) {
                            //    console.log(i + ' : ' +  data[i]);
                            //}
                        }
                    });
                }

                /*************************************************************************
                 ************************* grid remove event *****************************
                 *************************************************************************/
                // removeボタンがおされたとき
                if (event.target == 'remove') {
                    // selectには、選択されているgridのrecidが配列として、格納される。
                    var select = w2ui.tkMtgmain.getSelection();
                    console.log('sel.len: ', select.length);
                    // 選択されていなかったら、selectの長さは0
                    if(select.length == 1) {
                        var res = confirm('消去しますか？');
                        // OKボタンが押されたら
                        if(res == true) {
                            var sel = w2ui.tkMtgmain.getSelection();
                            console.log('sel', sel);
                            console.log('sel[0]: ', sel[0]);
                            var json_data = w2ui.tkMtgmain.get(sel[0]);
                            console.log('json_data: ', json_data);
                            var id = json_data.id;
                            console.log('send_id: ', id);
                            w2ui.tkMtgmain.remove(sel[0]);
                            $.ajax({
                                url: "/removeButton",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                data: JSON.stringify(id),
                                success: function (data) {
                                    //console.log('data: ', data);
                                    console.log('deleted_data: ', data);
                                    //for (var i in data) {
                                    //    console.log(i + ' : ' +  data[i]);
                                    //}
                                }
                            });
                            // 最後にフォームの内容をクリア
                            w2ui.tkMtfmain.clear();
                        }
                    }
                }
            }
        },
        records: [],
        /*************************************************************************
         ************************* grid click event *************************************
         *************************************************************************/
        onClick: function(event) {
            var grid = this;
            var form = w2ui.tkMtfmain;
            //console.log(event);
            event.onComplete = function() {
                var sel = grid.getSelection();
                //console.log(sel);
                if (sel.length == 1) {
                    form.recid = sel[0];
                    //alert('recid:' + form.recid);
                    //console.log(form.recid);
                    form.record = $.extend(true, {}, grid.get(sel[0]));
                    form.refresh();
                }
                else {
                    form.clear();
                }
            };
        }
    }
};