
$(function() {
    var unit_name = [];
    var sh_kubun_code_name = [];
    $(window).load(function(){
        $.ajax({
            url: "/",
            type: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json_data),
            success: function(data) {
                w2ui.tkMtgmain.records = data;
            }
        });

        $.ajax({
            url: "/tantoMt",
            type: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json_data),
            success: function(data) {
                w2ui.tantoMtgmain.records = data;
            }
        });
        $.ajax({
            url: "/shMt",
            type: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json_data),
            success: function(data) {
                w2ui.shMtgmain.records = data;
            }
        });
        $.ajax({
            url: "/shKubunMt",
            type: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json_data),
            success: function(data) {
                w2ui.shKubunMtgmain.records = data;

                // ここで、単位メンテから単位名を取ってきて、グローバル変数に格納。(正確には、グローバルではないと思うけど。)
                for (prop in data) {
                    console.log('商品区分コード', data[prop].shKubunCode);
                    console.log('商品区分名', data[prop].shKubunName);
                    sh_kubun_code_name.push({id:data[prop].shKubunCode, text:(data[prop].shKubunCode + ' : ' +  data[prop].shKubunName)});
                }

                console.log(sh_kubun_code_name);

            }
        });
        $.ajax({
            url: "/unitMt",
            type: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json_data),
            success: function(data) {
                w2ui.unitMtgmain.records = data;

                // ここで、単位メンテから単位名を取ってきて、グローバル変数に格納。(正確には、グローバルではないと思うけど。)
                for (prop in data) {
                    console.log(data[prop].unitName);
                    unit_name.push(data[prop].unitName);
                }
                console.log(unit_name);

                //w2ui.shMtfmain.fields.unit.options.items = unit_name;

            }
        });
        $.ajax({
            url: "/conTaxMt",
            type: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json_data),
            success: function(data) {
                w2ui.conTaxMtgmain.records = data;
            }
        });
        $.ajax({
            url: "/sysEnvMt",
            type: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json_data),
            success: function(data) {
                w2ui.sysEnvMtgmain.records = data;
            }
        });
        $.ajax({
            url: "/operMt",
            type: "POST",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json_data),
            success: function(data) {
               w2ui.operMtgmain.records = data;
           }
        });
    });
    // $("#main").w2layout(config.layout);
    // $().w2grid(config.grid);
    // $('#myForm').w2form(config.form);
    // read json file from local folder
    //w2ui.tkMttkMtgmain.load('../json/records.json');

    // w2ui.lmain.content('main', w2ui.tkMttkMtgmain);
    // w2ui.lmain.content('right', w2ui.tkMtfmain);
    $('#main').w2layout(config.layout);

    // 担当メンテ
    $().w2grid(tantoMt_grid_config);
    $().w2form(tantoMt_form_config);
    // 商品メンテ
    $().w2grid(shMt_grid_config);
    $().w2form(shMt_form_config);
    // ここで、単位メンテから商品メンテにデータを持ってくる。
    var op1 = {options: {items: unit_name}};
    w2ui.shMtfmain.set('unit', op1);
    w2ui.shMtfmain.refresh();
    // ここで、商品区分メンテから商品メンテにデータを持ってくる。
    var op2 = {options: {items: sh_kubun_code_name}};
    w2ui.shMtfmain.set('shKubunCode', op2);
    w2ui.shMtfmain.refresh();
    // 商品区分メンテ
    $().w2grid(shKubunMt_grid_config);
    $().w2form(shKubunMt_form_config);
    // 単位メンテ
    $().w2grid(unitMt_grid_config);
    $().w2form(unitMt_form_config);
    // 作業者メンテ
    $().w2grid(operMt_grid_config);
    $().w2form(operMt_form_config);
    // 消費税メンテ
    $().w2grid(conTaxMt_grid_config);
    $().w2form(conTaxMt_form_config);
    // システム環境メンテ
    $().w2grid(sysEnvMt_grid_config);
    $().w2form(sysEnvMt_form_config);

    w2ui.lmain.content('left', $().w2sidebar(config.sidebar));
    w2ui.lmain.content('main', $().w2grid(tkMt_grid_config));
    w2ui.lmain.content('right', $().w2form(tkMt_form_config));
});