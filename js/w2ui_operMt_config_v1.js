var operMt_grid_config = {
    name: 'operMtgmain',
    show: {
        toolbar: true,
        footer: true,
        lineNumbers: true
    },
    columns: [
        {field: 'recid', caption: 'recid', size: '60px', sortable: true, resizable: true, hidden: true},
        {field: 'id', caption: 'ID', size: '150px', sortable: true, resizable: true, hidden: true},
        {field: 'operCode', caption: '作業者コード', size: '20%', sortable: true, resizable: true},
        {field: 'operName', caption: '作業者名', size: '40%', sortable: true, resizable: true},
        {field: 'nickName', caption: '簡易名称', size: '40%', sortable: true, resizable: true},
    ],
    /*************************************************************************
     ************************* toolbar *************************************
     *************************************************************************/
    toolbar: {
        items: [
            {id: 'add', type: 'button', caption: 'データ行追加', icon: 'w2ui-icon-plus'},
            {id: 'remove', type: 'button', caption: 'データ行削除', icon: 'w2ui-icon-cross'}
        ],
        onClick: function (event) {
            /*************************************************************************
            ************************* grid add event *************************************
            *************************************************************************/
            if (event.target == 'add') {
                var max_recid = 0;
                for (var num in w2ui.operMtgmain.records) {
                    //console.log('num: ',num);
                    //console.log('content: ', w2ui.operMtgmain.records[num]);
                    //console.log('recid:', w2ui.operMtgmain.records[num].recid);
                    var recid_num = w2ui.operMtgmain.records[num].recid;
                    if(max_recid < recid_num) {
                        max_recid = recid_num;
                    }
                }
                //console.log('max', max_recid);

                w2ui.operMtgmain.add({recid: Number(max_recid) + 1});
                var json_data = w2ui.operMtgmain.get(Number(max_recid)+1);
                console.log(json_data);
                $.ajax({
                    url: "/operMt_addButton",
                    type: "POST",
                    async:false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(json_data),
                    success: function(data) {
                        //console.log('data: ', data);
                        console.log(data);
                        console.log(json_data);
                        json_data.id = data;
                        console.log(json_data);
                        // ページをリロードする感じ
                        w2ui.operMtgmain.refresh();
                        w2ui.operMtgmain.selectNone();
                        //for (var i in data) {
                        //    console.log(i + ' : ' +  data[i]);
                        //}
                    }
                });
            }

            /*************************************************************************
            ************************* grid remove event *****************************
            *************************************************************************/
            // removeボタンがおされたとき
            if (event.target == 'remove') {
                // selectには、選択されているgridのrecidが配列として、格納される。
                var select = w2ui.operMtgmain.getSelection();
                console.log('sel.len: ', select.length);
                // 選択されていなかったら、selectの長さは0
                if(select.length == 1) {
                    var res = confirm('消去しますか？');
                    // OKボタンが押されたら
                    if(res == true) {
                        var sel = w2ui.operMtgmain.getSelection();
                        console.log('sel', sel);
                        console.log('sel[0]: ', sel[0]);
                        var json_data = w2ui.operMtgmain.get(sel[0]);
                        console.log('json_data: ', json_data);
                        var id = json_data.id;
                        console.log('send_id: ', id);
                        w2ui.operMtgmain.remove(sel[0]);
                        $.ajax({
                            url: "/operMt_removeButton",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify(id),
                            success: function (data) {
                                //console.log('data: ', data);
                                console.log('deleted_data: ', data);
                                //for (var i in data) {
                                //    console.log(i + ' : ' +  data[i]);
                                //}
                            }
                        });
                        // 最後にフォームの内容をクリア
                        w2ui.operMtfmain.clear();
                    }
                }
            }
        }
    },
    //records: [],
    /*************************************************************************
     ************************* grid click event *************************************
     *************************************************************************/
    onClick: function(event) {
        var grid = this;
        var form = w2ui.operMtfmain;
        //console.log(event);
        event.onComplete = function() {
            var sel = grid.getSelection();
            //console.log(sel);
            if (sel.length == 1) {
                form.recid = sel[0];
                //alert('recid:' + form.recid);
                //console.log(form.recid);
                form.record = $.extend(true, {}, grid.get(sel[0]));
                form.refresh();
            }
            else {
                form.clear();
            }
        };
    }
};
var operMt_form_config = {
    name: 'operMtfmain',
    fields: [
        {name: 'recid', type: 'text', html: { caption: 'recid', attr: 'style="width:130px" readonly'}},
        {name: 'id', type: 'text', html: { caption: 'gaeid', attr: 'style="width:130px" readonly'}},
        {name: 'operCode', type: 'text', html: { caption: '作業者コード', attr: 'style="width:130px"'}},
        {name: 'operName', type: 'text', html: { caption: '作業者名', attr: 'style="width:200px"'}},
        {name: 'nickName', type: 'text', html: { caption: '簡易名称', attr: 'style="width:200px"'}},
        {name: 'yubinNo', type: 'text', html: { caption: '郵便番号', attr: 'style="width:130px"'}},
        {name: 'tkAddress_1', type: 'text', html: { caption: '住所-1', attr: 'style="width:200px"'}},
        {name: 'tkAddress_2', type: 'text', html: { caption: '住所-2', attr: 'style="width:200px"'}},
        {name: 'telNo', type: 'text', html: { caption: '電話番号', attr: 'style="width:130px"'}},
        {name: 'birthDate', type: 'date', html: { caption: '生年月日', attr: 'style="width:130px"'}},
        {name: 'memo', type: 'textarea', html: { caption: 'メモ', attr: 'style="width:200px; height:100px"'}}
    ],
    actions: {
        /*************************************************************************
         ************************* form reset *************************************
         *************************************************************************/
        Reset: function () {
            this.clear();
        },
        /*************************************************************************
         ************************* form save *************************************
         *************************************************************************/
        Save: function () {
            var errors = this.validate();
            if (errors.length > 0) return;

            if (this.recid != 0) {
                //w2ui.operMtgmain.add({recid: w2ui.operMtgmain.records.length+1});
                //console.log('recid: ' + this.recid + '\nrecord: ' + this.record);
                w2ui.operMtgmain.set(this.recid, this.record);
                w2ui.operMtgmain.selectNone();
                //console.log('saved target: ', this.record);
                //for (var i in this.record) {
                //    console.log('content: ',this.record[i]);
                //}

                var json_data = this.record;
                console.log('json_data: ', json_data);
                $.ajax({
                    url: "/operMt_saveButton",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(json_data),
                    success: function (data) {
                        //console.log('data: ', data);
                        console.log('this data is saved: ', data);
                        //for (var i in data) {
                        //    console.log(i + ' : ' +  data[i]);
                        //}
                    }
                });
                this.clear();
            } else {
                alert('行を選択してください。');
            }
        }
    }
};
