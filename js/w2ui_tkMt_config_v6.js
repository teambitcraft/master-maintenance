
var tkMt_grid_config = {
    name: 'tkMtgmain',
    show: {
        toolbar: true,
        footer: true,
        lineNumbers: true
    },
    columns: [
        {field: 'recid', caption: 'recid', size: '60px', sortable: true, resizable: true, hidden: true},
        {field: 'id', caption: 'ID', size: '150px', sortable: true, resizable: true, hidden: true},
        //{field: 'id', caption: 'ID', size: '150px', sortable: true, resizable: true, hidden:true},
        {field: 'tkCode', caption: '得意先コード', size: '30%', sortable: true, resizable: true},
        {field: 'tkName', caption: '得意先名', size: '30%', sortable: true, resizable: true},
        {field: 'tkAddress', caption: '得意先住所', size: '40%', sortable: true, resizable: true},
        {field: 'telNo', caption: '電話番号', size: '120px', sortable: true, resizable: true}
    ],
    /*************************************************************************
     ************************* toolbar *************************************
     *************************************************************************/
    toolbar: {
        items: [
            {id: 'add', type: 'button', caption: 'データ行追加', icon: 'w2ui-icon-plus'},
            {id: 'remove', type: 'button', caption: 'データ行削除', icon: 'w2ui-icon-cross'}
        ],
        onClick: function (event) {
            /*************************************************************************
            ************************* grid add event *************************************
            *************************************************************************/
            if (event.target == 'add') {
                var max_recid = 0;
                for (var num in w2ui.tkMtgmain.records) {
                    //console.log('num: ',num);
                    //console.log('content: ', w2ui.tkMtgmain.records[num]);
                    //console.log('recid:', w2ui.tkMtgmain.records[num].recid);
                    var recid_num = w2ui.tkMtgmain.records[num].recid;
                    if(max_recid < recid_num) {
                        max_recid = recid_num;
                    }
                }
                //console.log('max', max_recid);
        
                w2ui.tkMtgmain.add({recid: Number(max_recid) + 1});
                var json_data = w2ui.tkMtgmain.get(Number(max_recid)+1);
                console.log(json_data);
                $.ajax({
                    url: "/tkMt_addButton",
                    type: "POST",
                    async:false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(json_data),
                    success: function(data) {
                        //console.log('data: ', data);
                        console.log(data);
                        console.log(json_data);
                        json_data.id = data;
                        console.log(json_data);
                        // ページをリロードする感じ
                        w2ui.tkMtgmain.refresh();
                        w2ui.tkMtgmain.selectNone();
                        //for (var i in data) {
                        //    console.log(i + ' : ' +  data[i]);
                        //}
                    }
                });
            }
        
            /*************************************************************************
            ************************* grid remove event *****************************
            *************************************************************************/
            // removeボタンがおされたとき
            if (event.target == 'remove') {
                // selectには、選択されているgridのrecidが配列として、格納される。
                var select = w2ui.tkMtgmain.getSelection();
                console.log('sel.len: ', select.length);
                // 選択されていなかったら、selectの長さは0
                if(select.length == 1) {
                    var res = confirm('消去しますか？');
                    // OKボタンが押されたら
                    if(res == true) {
                        var sel = w2ui.tkMtgmain.getSelection();
                        console.log('sel', sel);
                        console.log('sel[0]: ', sel[0]);
                        var json_data = w2ui.tkMtgmain.get(sel[0]);
                        console.log('json_data: ', json_data);
                        var id = json_data.id;
                        console.log('send_id: ', id);
                        w2ui.tkMtgmain.remove(sel[0]);
                        $.ajax({
                            url: "/tkMt_removeButton",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify(id),
                            success: function (data) {
                                //console.log('data: ', data);
                                console.log('deleted_data: ', data);
                                //for (var i in data) {
                                //    console.log(i + ' : ' +  data[i]);
                                //}
                            }
                        });
                        // 最後にフォームの内容をクリア
                        w2ui.tkMtfmain.clear();
                    }
                }
            }
        }
    },
    //records: [],
    /*************************************************************************
     ************************* grid click event *************************************
     *************************************************************************/
    onClick: function(event) {
        var grid = this;
        var form = w2ui.tkMtfmain;
        //console.log(event);
        event.onComplete = function() {
            var sel = grid.getSelection();
            //console.log(sel);
            if (sel.length == 1) {
                form.recid = sel[0];
                //alert('recid:' + form.recid);
                //console.log(form.recid);
                form.record = $.extend(true, {}, grid.get(sel[0]));
                form.refresh();
            }
            else {
                form.clear();
            }
        };
    }
};
var tkMt_form_config = {
    name: 'tkMtfmain',
    fields: [
        {name: 'recid', type: 'text', html: { caption: 'recid', attr: 'style="width:130px" readonly'}},
        {name: 'id', type: 'text', html: { caption: 'gaeid', attr: 'style="width:130px" readonly'}},
        {name: 'tkCode', type: 'text', html: { caption: '得意先コード', attr: 'style="width: 200px"'}},
        {name: 'tkName', type: 'text', html: { caption: '得意先名', attr: 'style="width: 200px"'}},
        {name: 'nickName', type: 'text', html: { caption: '簡易名称', attr: 'style="width: 200px"'}},
        {name: 'gojuOn', type: 'text', html: { caption: '頭1文字', attr: 'style="width: 200px"'}},
        {name: 'yubinNo', type: 'text', html: { caption: '郵便番号', attr: 'style="width: 200px"'}},
        {name: 'tkAddress', type: 'text', html: { caption: '得意先住所', attr: 'style="width: 200px"'}},
        {name: 'telNo', type: 'text', html: { caption: '電話番号', attr: 'style="width: 200px"'}},
        {name: 'faxNo', type: 'text', html: { caption: 'FAX番号', attr: 'style="width: 200px"'}},
        {name: 'tkKubun', type: 'list', required: true, html: { caption: '得意先区分', attr: 'style="width: 100px"'}, options: { items: ['得意先', '外注先', '仕入先', 'リース', '燃料', '修理'] }},
        {name: 'uriShimeDate', type: 'text', html: { caption: '売上締日', attr: 'style="width: 200px"'}},
        {name: 'shukinDate', type: 'text', html: { caption: '集金日', attr: 'style="width: 200px"'}},
        {name: 'tekiyoTanka', type: 'list', required: true, html: { caption: '適用単価', attr: 'style="width: 100px"'}, options: { items: ['標準単価', '単価1', '単価2', '単価3', '単価4', '単価5'] }},
        {name: 'seikyuPrintJun', type: 'list', required: true, html: { caption: '請求明細印刷順', attr: 'style="width: 100px"'}, options: { items: ['標準（日付順）', '現場順', '担当者順'] }},
        {name: 'memo', type: 'textarea', html: { caption: 'メモ', attr: 'style="width: 200px; height=100px"'}}
    ],
    actions: {
        /*************************************************************************
         ************************* form reset *************************************
         *************************************************************************/
        Reset: function () {
            this.clear()
        },
        /*************************************************************************
         ************************* form save *************************************
         *************************************************************************/
        Save: function () {
            var errors = this.validate();
            if (errors.length > 0) return;

            if (this.recid != 0) {
                //w2ui.tkMtgmain.add({recid: w2ui.tkMtgmain.records.length+1});
                //console.log('recid: ' + this.recid + '\nrecord: ' + this.record);
                w2ui.tkMtgmain.set(this.recid, this.record);
                w2ui.tkMtgmain.selectNone();
                //console.log('saved target: ', this.record);
                //for (var i in this.record) {
                //    console.log('content: ',this.record[i]);
                //}

                var json_data = this.record;
                console.log('json_data: ', json_data);
                $.ajax({
                    url: "/tkMt_saveButton",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(json_data),
                    success: function (data) {
                        //console.log('data: ', data);
                        console.log('this data is saved: ', data);
                        //for (var i in data) {
                        //    console.log(i + ' : ' +  data[i]);
                        //}
                    }
                });
                this.clear();
            } else {
                alert('行を選択してください。');
            }
        }
    }
};


// config["grid"] = grid_config;
// config["form"] = form_config;
// grid_config.toolbar.onClick = grid_toolbar_click;
// config["grid"] = grid_config;
// config["form"] = form_config;