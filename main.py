#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import json
import os
from google.appengine.ext.webapp import template
from google.appengine.ext import db
from google.appengine.api import users

# from django.utils import json
from google.appengine.ext import blobstore


#########################################################################
########################### inheritance #################################
#########################################################################
class DictModel(db.Model):
    def to_dict(self):
        return dict([(p, unicode(getattr(self, p))) for p in self.properties()
                     ])


class DictModel2(db.Model):
    def to_dict(self):
        elm_list = []
        for p in self.properties():
            if p == 'recid' or 'id':
                elm_list.append((p, getattr(self, p)))
            else:
                elm_list.append((p, unicode(getattr(self, p))))
        return dict(elm_list)

###################################################################################################
########################### 得意先メンテ　###########################################################
###################################################################################################


class tkDB(DictModel2):

    recid = db.IntegerProperty()
    id = db.IntegerProperty()
    tkCode = db.StringProperty(multiline=False)
    tkName = db.StringProperty(multiline=False)
    nickName = db.StringProperty(multiline=False)
    gojuOn = db.StringProperty(multiline=False)
    yubinNo = db.StringProperty(multiline=False)
    tkAddress = db.StringProperty(multiline=False)
    telNo = db.StringProperty(multiline=False)
    faxNo = db.StringProperty(multiline=False)
    tkKubun = db.StringProperty(multiline=False)
    uriShimeDate = db.StringProperty(multiline=False)
    shukinDate = db.StringProperty(multiline=False)
    tekiyoTanka = db.StringProperty(multiline=False)
    seikyuPrintJun = db.StringProperty(multiline=False)
    memo = db.StringProperty(multiline=True)


class tkMt_MainHandler(webapp2.RequestHandler):
    def get(self):
        user = users.is_current_user_admin()
        if user:
            params = {}
            fpath = os.path.join(
                os.path.dirname(__file__), 'layouts', 'layout_v4.html')
            html = template.render(fpath, params)
            self.response.out.write(html)
        else:
            self.redirect(users.create_login_url(self.request.uri))

    def post(self):
        if users.is_current_user_admin():
            datas = tkDB.gql('order by tkCode')
            self.response.out.write(json.dumps([p.to_dict() for p in datas]))
        else:
            self.redrect('/')


class tkMt_SaveHandler(webapp2.RequestHandler):
    def post(self):
        json_data = self.request.body
        obj = json.loads(json_data)
        id = int(obj['id'])
        data = tkDB.get_by_id(long(id))
        data.tkCode = obj['tkCode']
        data.tkName = obj['tkName']
        data.nickName = obj['nickName']
        data.gojuOn = obj['gojuOn']
        data.yubinNo = obj['yubinNo']
        data.tkAddress = obj['tkAddress']
        data.telNo = obj['telNo']
        data.faxNo = obj['faxNo']
        # ここだけunicode. utf-8にしたら駄目だった。optionsはobjectになっているから、['text']と付け加えないといけない。
        data.tkKubun = obj['tkKubun']['text']
        data.uriShimeDate = obj['uriShimeDate']
        data.shukinDate = obj['shukinDate']
        data.tekiyoTanka = obj['tekiyoTanka']['text']
        data.seikyuPrintJun = obj['seikyuPrintJun']['text']
        data.memo = obj['memo']
        data.put()
        # self.redirect('/')
        # self.response.content_type = 'application/json; charset=utf-8'
        self.response.out.write(id)


class tkMt_AddHandler(webapp2.RequestHandler):
    def post(self):

        # json_data = self.request.body
        # # jsonをオブジェクトに
        # obj = json.loads(json_data)
        # obj['recid'] = str(obj['recid']) + " via server."
        #
        # #content-typeは application/json
        # self.response.content_type = 'application/json; charset=utf-8'
        # #　文字列に変換して返す
        # self.response.out.write(json.dumps(obj))
        json_data = self.request.body
        obj = json.loads(json_data)
        recid = int(obj['recid'])
        # tkAddress = obj['tkAddress']
        # tkName = obj['tkName']
        # telNo = obj['telNo']

        data = tkDB(recid=recid)
        # data.tkAddress = tkAddress
        # data.tkName = tkName
        # data.telNo = telNo
        data.put()
        self.response.out.write(data.key().id())
        # self.redirect('/')
        data_2 = tkDB.get_by_id(data.key().id())
        data_2.id = data.key().id()
        data_2.put()


class tkMt_RemoveHandler(webapp2.RequestHandler):
    def post(self):
        id = self.request.body
        data = tkDB.get_by_id(long(id))
        data.delete()
        self.response.out.write(id)

#######################################################################
######################### 担当メンテ ####################################
#######################################################################


class tantoDB(DictModel2):
    recid = db.IntegerProperty()
    id = db.IntegerProperty()
    tantoCode = db.StringProperty(multiline=False)
    tantoName = db.StringProperty(multiline=False)
    memo = db.StringProperty(multiline=True)


class tantoMt_MainHandler(webapp2.RequestHandler):
    def post(self):
        datas = tantoDB.gql('order by tantoCode')
        self.response.out.write(json.dumps([p.to_dict() for p in datas]))


class tantoMt_AddHandler(webapp2.RequestHandler):
    def post(self):

        # json_data = self.request.body
        # # jsonをオブジェクトに
        # obj = json.loads(json_data)
        # obj['recid'] = str(obj['recid']) + " via server."
        #
        # #content-typeは application/json
        # self.response.content_type = 'application/json; charset=utf-8'
        # #　文字列に変換して返す
        # self.response.out.write(json.dumps(obj))
        json_data = self.request.body
        obj = json.loads(json_data)
        recid = int(obj['recid'])
        # tkAddress = obj['tkAddress']
        # tkName = obj['tkName']
        # telNo = obj['telNo']

        data = tantoDB(recid=recid)
        # data.tkAddress = tkAddress
        # data.tkName = tkName
        # data.telNo = telNo
        data.put()
        self.response.out.write(data.key().id())
        data_2 = tantoDB.get_by_id(data.key().id())
        data_2.id = data.key().id()
        data_2.put()


class tantoMt_SaveHandler(webapp2.RequestHandler):
    def post(self):
        json_data = self.request.body
        obj = json.loads(json_data)
        id = int(obj['id'])
        data = tantoDB.get_by_id(long(id))
        data.tantoCode = obj['tantoCode']
        data.tantoName = obj['tantoName']
        data.memo = obj['memo']
        data.put()
        # self.redirect('/')
        # self.response.content_type = 'application/json; charset=utf-8'
        self.response.out.write(id)


class tantoMt_RemoveHandler(webapp2.RequestHandler):
    def post(self):
        id = self.request.body
        data = tantoDB.get_by_id(long(id))
        data.delete()
        self.response.out.write(id)

###################################################################################################
########################### 商品メンテ　###########################################################
###################################################################################################


class shDB(DictModel2):

    recid = db.IntegerProperty()
    id = db.IntegerProperty()
    shKubunCode = db.StringProperty(multiline=False)
    shCode = db.StringProperty(multiline=False)
    sharyoNo = db.StringProperty(multiline=False)
    shName = db.StringProperty(multiline=False)
    unit = db.StringProperty(multiline=False)
    makerName = db.StringProperty(multiline=False)
    kataShiki = db.StringProperty(multiline=False)
    seizoNo = db.StringProperty(multiline=False)
    shisanNo = db.StringProperty(multiline=False)
    ryakuName_1 = db.StringProperty(multiline=False)
    ryakuName_2 = db.StringProperty(multiline=False)
    inputKubun = db.StringProperty(multiline=False)
    memo = db.StringProperty(multiline=True)
    dayStdPrice = db.StringProperty(multiline=False)
    dayTanka_1 = db.StringProperty(multiline=False)
    dayTanka_2 = db.StringProperty(multiline=False)
    dayTanka_3 = db.StringProperty(multiline=False)
    dayTanka_4 = db.StringProperty(multiline=False)
    dayTanka_5 = db.StringProperty(multiline=False)
    eveStdPrice = db.StringProperty(multiline=False)
    eveTanka_1 = db.StringProperty(multiline=False)
    eveTanka_2 = db.StringProperty(multiline=False)
    eveTanka_3 = db.StringProperty(multiline=False)
    eveTanka_4 = db.StringProperty(multiline=False)
    eveTanka_5 = db.StringProperty(multiline=False)


class shMt_MainHandler(webapp2.RequestHandler):
    def get(self):
        params = {}
        fpath = os.path.join(
            os.path.dirname(__file__), 'layouts', 'layout_v4.html')
        html = template.render(fpath, params)
        self.response.out.write(html)

    def post(self):
        datas = shDB.gql('order by shKubunCode')
        self.response.out.write(json.dumps([p.to_dict() for p in datas]))


class shMt_SaveHandler(webapp2.RequestHandler):
    def post(self):
        json_data = self.request.body
        obj = json.loads(json_data)
        id = int(obj['id'])
        data = shDB.get_by_id(long(id))
        data.shKubunCode = obj['shKubunCode']['id']
        data.shCode = obj['shCode']
        data.sharyoNo = obj['sharyoNo']
        data.shName = obj['shName']
        data.unit = obj['unit']['text']
        data.makerName = obj['makerName']
        data.kataShiki = obj['kataShiki']
        data.seizoNo = obj['seizoNo']
        data.shisanNo = obj['shisanNo']
        data.ryakuName_1 = obj['ryakuName_1']
        data.ryakuName_2 = obj['ryakuName_2']
        data.inputKubun = obj['inputKubun']['text']
        data.memo = obj['memo']
        data.dayStdPrice = obj['dayStdPrice']
        data.dayTanka_1 = obj['dayTanka_1']
        data.dayTanka_2 = obj['dayTanka_2']
        data.dayTanka_3 = obj['dayTanka_3']
        data.dayTanka_4 = obj['dayTanka_4']
        data.dayTanka_5 = obj['dayTanka_5']
        data.eveStdPrice = obj['eveStdPrice']
        data.eveTanka_1 = obj['eveTanka_1']
        data.eveTanka_2 = obj['eveTanka_2']
        data.eveTanka_3 = obj['eveTanka_3']
        data.eveTanka_4 = obj['eveTanka_4']
        data.eveTanka_5 = obj['eveTanka_5']
        data.put()
        # self.redirect('/')
        # self.response.content_type = 'application/json; charset=utf-8'
        self.response.out.write(id)


class shMt_AddHandler(webapp2.RequestHandler):
    def post(self):

        # json_data = self.request.body
        # # jsonをオブジェクトに
        # obj = json.loads(json_data)
        # obj['recid'] = str(obj['recid']) + " via server."
        #
        # #content-typeは application/json
        # self.response.content_type = 'application/json; charset=utf-8'
        # #　文字列に変換して返す
        # self.response.out.write(json.dumps(obj))
        json_data = self.request.body
        obj = json.loads(json_data)
        recid = int(obj['recid'])
        # shAddress = obj['shAddress']
        # shName = obj['shName']
        # telNo = obj['telNo']

        data = shDB(recid=recid)
        # data.shAddress = shAddress
        # data.shName = shName
        # data.telNo = telNo
        data.put()
        self.response.out.write(data.key().id())
        # self.redirect('/')
        data_2 = shDB.get_by_id(data.key().id())
        data_2.id = data.key().id()
        data_2.put()


class shMt_RemoveHandler(webapp2.RequestHandler):
    def post(self):
        id = self.request.body
        data = shDB.get_by_id(long(id))
        data.delete()
        self.response.out.write(id)


############################################################
##################### 商品区分メンテ ##########################
############################################################
class shKubunDB(DictModel2):
    recid = db.IntegerProperty()
    id = db.IntegerProperty()
    shKubunCode = db.StringProperty(multiline=False)
    shKubunName = db.StringProperty(multiline=False)
    memo = db.StringProperty(multiline=True)


class shKubunMt_MainHandler(webapp2.RequestHandler):
    def post(self):
        datas = shKubunDB.gql('order by shKubunCode')
        self.response.out.write(json.dumps([p.to_dict() for p in datas]))


class shKubunMt_AddHandler(webapp2.RequestHandler):
    def post(self):

        # json_data = self.request.body
        # # jsonをオブジェクトに
        # obj = json.loads(json_data)
        # obj['recid'] = str(obj['recid']) + " via server."
        #
        # #content-typeは application/json
        # self.response.content_type = 'application/json; charset=utf-8'
        # #　文字列に変換して返す
        # self.response.out.write(json.dumps(obj))
        json_data = self.request.body
        obj = json.loads(json_data)
        recid = int(obj['recid'])
        # tkAddress = obj['tkAddress']
        # tkName = obj['tkName']
        # telNo = obj['telNo']

        data = shKubunDB(recid=recid)
        # data.tkAddress = tkAddress
        # data.tkName = tkName
        # data.telNo = telNo
        data.put()
        self.response.out.write(data.key().id())
        data_2 = shKubunDB.get_by_id(data.key().id())
        data_2.id = data.key().id()
        data_2.put()


class shKubunMt_SaveHandler(webapp2.RequestHandler):
    def post(self):
        json_data = self.request.body
        obj = json.loads(json_data)
        id = int(obj['id'])
        data = shKubunDB.get_by_id(long(id))
        data.shKubunCode = obj['shKubunCode']
        data.shKubunName = obj['shKubunName']
        data.memo = obj['memo']
        data.put()
        # self.redirect('/')
        # self.response.content_type = 'application/json; charset=utf-8'
        self.response.out.write(id)


class shKubunMt_RemoveHandler(webapp2.RequestHandler):
    def post(self):
        id = self.request.body
        data = shKubunDB.get_by_id(long(id))
        data.delete()
        self.response.out.write(id)


#########################################################################
#################### 単位メンテ ##########################################
#########################################################################
class unitDB(DictModel2):
    recid = db.IntegerProperty()
    id = db.IntegerProperty()
    unitCode = db.StringProperty(multiline=False)
    unitName = db.StringProperty(multiline=False)
    memo = db.StringProperty(multiline=True)


class unitMt_MainHandler(webapp2.RequestHandler):
    def post(self):
        datas = unitDB.gql('order by unitCode')
        self.response.out.write(json.dumps([p.to_dict() for p in datas]))


class unitMt_AddHandler(webapp2.RequestHandler):
    def post(self):

        # json_data = self.request.body
        # # jsonをオブジェクトに
        # obj = json.loads(json_data)
        # obj['recid'] = str(obj['recid']) + " via server."
        #
        # #content-typeは application/json
        # self.response.content_type = 'application/json; charset=utf-8'
        # #　文字列に変換して返す
        # self.response.out.write(json.dumps(obj))
        json_data = self.request.body
        obj = json.loads(json_data)
        recid = int(obj['recid'])
        # tkAddress = obj['tkAddress']
        # tkName = obj['tkName']
        # telNo = obj['telNo']

        data = unitDB(recid=recid)
        # data.tkAddress = tkAddress
        # data.tkName = tkName
        # data.telNo = telNo
        data.put()
        self.response.out.write(data.key().id())
        data_2 = unitDB.get_by_id(data.key().id())
        data_2.id = data.key().id()
        data_2.put()


class unitMt_SaveHandler(webapp2.RequestHandler):
    def post(self):
        json_data = self.request.body
        obj = json.loads(json_data)
        id = int(obj['id'])
        data = unitDB.get_by_id(long(id))
        data.unitCode = obj['unitCode']
        data.unitName = obj['unitName']
        data.memo = obj['memo']
        data.put()
        # self.redirect('/')
        # self.response.content_type = 'application/json; charset=utf-8'
        self.response.out.write(id)


class unitMt_RemoveHandler(webapp2.RequestHandler):
    def post(self):
        id = self.request.body
        data = unitDB.get_by_id(long(id))
        data.delete()
        self.response.out.write(id)


#########################################################################
#################### 作業者メンテ ##########################################
#########################################################################
class operDB(DictModel2):
    recid = db.IntegerProperty()
    id = db.IntegerProperty()
    operCode = db.StringProperty(multiline=False)
    operName = db.StringProperty(multiline=False)
    nickName = db.StringProperty(multiline=False)
    yubinNo = db.StringProperty(multiline=False)
    tkAddress_1 = db.StringProperty(multiline=False)
    tkAddress_2 = db.StringProperty(multiline=False)
    telNo = db.StringProperty(multiline=False)
    birthDate = db.StringProperty(multiline=False)
    memo = db.StringProperty(multiline=True)


class operMt_MainHandler(webapp2.RequestHandler):
    def post(self):
        datas = operDB.gql('order by operCode')
        self.response.out.write(json.dumps([p.to_dict() for p in datas]))


class operMt_AddHandler(webapp2.RequestHandler):
    def post(self):

        # json_data = self.request.body
        # # jsonをオブジェクトに
        # obj = json.loads(json_data)
        # obj['recid'] = str(obj['recid']) + " via server."
        #
        # #content-typeは application/json
        # self.response.content_type = 'application/json; charset=utf-8'
        # #　文字列に変換して返す
        # self.response.out.write(json.dumps(obj))
        json_data = self.request.body
        obj = json.loads(json_data)
        recid = int(obj['recid'])
        # tkAddress = obj['tkAddress']
        # tkName = obj['tkName']
        # telNo = obj['telNo']

        data = operDB(recid=recid)
        # data.tkAddress = tkAddress
        # data.tkName = tkName
        # data.telNo = telNo
        data.put()
        self.response.out.write(data.key().id())
        data_2 = operDB.get_by_id(data.key().id())
        data_2.id = data.key().id()
        data_2.put()


class operMt_SaveHandler(webapp2.RequestHandler):
    def post(self):
        json_data = self.request.body
        obj = json.loads(json_data)
        id = int(obj['id'])
        data = operDB.get_by_id(long(id))
        data.operCode = obj['operCode']
        data.operName = obj['operName']
        data.nickName = obj['nickName']
        data.yubinNo = obj['yubinNo']
        data.tkAddress_1 = obj['tkAddress_1']
        data.tkAddress_2 = obj['tkAddress_2']
        data.telNo = obj['telNo']
        data.birthDate = obj['birthDate']
        data.memo = obj['memo']
        data.put()
        # self.redirect('/')
        # self.response.content_type = 'application/json; charset=utf-8'
        self.response.out.write(id)


class operMt_RemoveHandler(webapp2.RequestHandler):
    def post(self):
        id = self.request.body
        data = operDB.get_by_id(long(id))
        data.delete()
        self.response.out.write(id)


#########################################################################
#################### 消費税メンテ ##########################################
#########################################################################
class conTaxDB(DictModel2):
    recid = db.IntegerProperty()
    id = db.IntegerProperty()
    histNo = db.StringProperty(multiline=False)
    startDate = db.StringProperty(multiline=False)
    percent = db.StringProperty(multiline=False)


class conTaxMt_MainHandler(webapp2.RequestHandler):
    def post(self):
        datas = conTaxDB.gql('order by histNo')
        self.response.out.write(json.dumps([p.to_dict() for p in datas]))


class conTaxMt_AddHandler(webapp2.RequestHandler):
    def post(self):

        # json_data = self.request.body
        # # jsonをオブジェクトに
        # obj = json.loads(json_data)
        # obj['recid'] = str(obj['recid']) + " via server."
        #
        # #content-typeは application/json
        # self.response.content_type = 'application/json; charset=utf-8'
        # #　文字列に変換して返す
        # self.response.out.write(json.dumps(obj))
        json_data = self.request.body
        obj = json.loads(json_data)
        recid = int(obj['recid'])
        # tkAddress = obj['tkAddress']
        # tkName = obj['tkName']
        # telNo = obj['telNo']

        data = conTaxDB(recid=recid)
        # data.tkAddress = tkAddress
        # data.tkName = tkName
        # data.telNo = telNo
        data.put()
        self.response.out.write(data.key().id())
        data_2 = conTaxDB.get_by_id(data.key().id())
        data_2.id = data.key().id()
        data_2.put()


class conTaxMt_SaveHandler(webapp2.RequestHandler):
    def post(self):
        json_data = self.request.body
        obj = json.loads(json_data)
        id = int(obj['id'])
        data = conTaxDB.get_by_id(long(id))
        data.histNo = obj['histNo']
        data.startDate = obj['startDate']
        data.percent = obj['percent']
        data.put()
        # self.redirect('/')
        # self.response.content_type = 'application/json; charset=utf-8'
        self.response.out.write(id)


class conTaxMt_RemoveHandler(webapp2.RequestHandler):
    def post(self):
        id = self.request.body
        data = conTaxDB.get_by_id(long(id))
        data.delete()
        self.response.out.write(id)


#########################################################################
#################### システム環境メンテ #####################################
#########################################################################
class sysEnvDB(DictModel2):
    recid = db.IntegerProperty()
    id = db.IntegerProperty()
    dispYear = db.StringProperty(multiline=False)
    fisStartDate = db.StringProperty(multiline=False)
    fisEndDate = db.StringProperty(multiline=False)


class sysEnvMt_MainHandler(webapp2.RequestHandler):
    def post(self):
        datas = sysEnvDB.gql('order by recid')
        self.response.out.write(json.dumps([p.to_dict() for p in datas]))


class sysEnvMt_AddHandler(webapp2.RequestHandler):
    def post(self):

        # json_data = self.request.body
        # # jsonをオブジェクトに
        # obj = json.loads(json_data)
        # obj['recid'] = str(obj['recid']) + " via server."
        #
        # #content-typeは application/json
        # self.response.content_type = 'application/json; charset=utf-8'
        # #　文字列に変換して返す
        # self.response.out.write(json.dumps(obj))
        json_data = self.request.body
        obj = json.loads(json_data)
        recid = int(obj['recid'])
        # tkAddress = obj['tkAddress']
        # tkName = obj['tkName']
        # telNo = obj['telNo']

        data = sysEnvDB(recid=recid)
        # data.tkAddress = tkAddress
        # data.tkName = tkName
        # data.telNo = telNo
        data.put()
        self.response.out.write(data.key().id())
        data_2 = sysEnvDB.get_by_id(data.key().id())
        data_2.id = data.key().id()
        data_2.put()


class sysEnvMt_SaveHandler(webapp2.RequestHandler):
    def post(self):
        json_data = self.request.body
        obj = json.loads(json_data)
        id = int(obj['id'])
        data = sysEnvDB.get_by_id(long(id))
        data.dispYear = obj['dispYear']
        data.fisStartDate = obj['fisStartDate']
        data.fisEndDate = obj['fisEndDate']
        data.put()
        # self.redirect('/')
        # self.response.content_type = 'application/json; charset=utf-8'
        self.response.out.write(id)


class sysEnvMt_RemoveHandler(webapp2.RequestHandler):
    def post(self):
        id = self.request.body
        data = sysEnvDB.get_by_id(long(id))
        data.delete()
        self.response.out.write(id)


app = webapp2.WSGIApplication(
    [
        ('/', tkMt_MainHandler),
        ('/tkMt_saveButton', tkMt_SaveHandler),
        ('/tkMt_addButton', tkMt_AddHandler),
        ('/tkMt_removeButton', tkMt_RemoveHandler),
        ('/tantoMt', tantoMt_MainHandler),
        ('/tantoMt_saveButton', tantoMt_SaveHandler),
        ('/tantoMt_addButton', tantoMt_AddHandler),
        ('/tantoMt_removeButton', tantoMt_RemoveHandler),
        ('/shMt', shMt_MainHandler),
        ('/shMt_saveButton', shMt_SaveHandler),
        ('/shMt_addButton', shMt_AddHandler),
        ('/shMt_removeButton', shMt_RemoveHandler),
        ('/shKubunMt', shKubunMt_MainHandler),
        ('/shKubunMt_saveButton', shKubunMt_SaveHandler),
        ('/shKubunMt_addButton', shKubunMt_AddHandler),
        ('/shKubunMt_removeButton', shKubunMt_RemoveHandler),
        ('/unitMt', unitMt_MainHandler),
        ('/unitMt_saveButton', unitMt_SaveHandler),
        ('/unitMt_addButton', unitMt_AddHandler),
        ('/unitMt_removeButton', unitMt_RemoveHandler),
        ('/operMt', operMt_MainHandler),
        ('/operMt_saveButton', operMt_SaveHandler),
        ('/operMt_addButton', operMt_AddHandler),
        ('/operMt_removeButton', operMt_RemoveHandler),
        ('/conTaxMt', conTaxMt_MainHandler),
        ('/conTaxMt_saveButton', conTaxMt_SaveHandler),
        ('/conTaxMt_addButton', conTaxMt_AddHandler),
        ('/conTaxMt_removeButton', conTaxMt_RemoveHandler),
        ('/sysEnvMt', sysEnvMt_MainHandler),
        ('/sysEnvMt_saveButton', sysEnvMt_SaveHandler),
        ('/sysEnvMt_addButton', sysEnvMt_AddHandler),
        ('/sysEnvMt_removeButton', sysEnvMt_RemoveHandler),
    ],
    debug=True)
